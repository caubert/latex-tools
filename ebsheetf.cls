% vim: syntax=tex

% Classe de documents pour feuilles d'exercices en français (TD, devoirs,
% corrections...). Voir 'ebsheet.cls' pour le mode d'emploi.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ebsheetf}[2008/09/04 EB's format for exercise sheets, in French]

\LoadClassWithOptions{ebsheet}

\RequirePackage[frenchb]{babel}
%\FrenchItemizeSpacingfalse

\renewcommand\exercisename{Exercice}
\renewcommand\hintname{Indication}
\renewcommand\answername{Correction}

\renewcommand\definitionname{D\'efinition}
\renewcommand\notationname{Notation}
\renewcommand\examplename{Exemple}
\renewcommand\remarkname{Remarque}
\renewcommand\theoremname{Th\'eor\`eme}
\renewcommand\propositionname{Proposition}
\renewcommand\lemmaname{Lemme}
\renewcommand\corollaryname{Corollaire}
